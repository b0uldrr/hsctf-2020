## HSCTF 2020

* **CTF Time page:** https://ctftime.org/event/939
* **Category:** jeopardy
* **Date:** Mon, 01 June 2020, 12:00 UTC — Sat, 06 June 2020, 00:00 UTC

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|boredom|binary|100|bof, 64 bit|
|broken tokens|web|100|jwt|
|debt simulator|web|100|javascript, post, curl|
|ice cream bytes|rev|100|java|
|recursion reverse|rev|100|java, brute force|
|very safe login|web|100|source code|


### Unsolved Challenges to Revisit
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|pwnagotchi|binary|116|bof, rop, ret2libc|
