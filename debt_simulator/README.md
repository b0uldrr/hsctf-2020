## Debt Simulator

* **CTF:** hsctf 2020 
* **Category:** web
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 04/06/20

---

### Challenge
```
https://debt-simulator.web.hsctf.com/

Author: Madeleine
```

---

### Solution

The challenge site was a simple interface with a single button which you could press to play a debt simulator. Press the button enough and you would run out of money and the would restart. 

![main.png](images/main.png)

I looked through the source documents and found `App.js`, which held the functionality for clicking the button. Whenever the button was pressed, the script would make a post to `https://debt-simulator-login-backend.web.hsctf.com/yolo_0000000000001`.

![1.png](images/1.png)

Looking at that resource, it lists a function of `getgetgetgetgetgetgetgetgetFlag`, but scanning through the rest of the source code, there was no way to call this function in the program.

![2.png](images/2.png)

I issued my own post to the resource using `curl`, and this returned the flag.

```
$ curl --data "function=getgetgetgetgetgetgetgetgetFlag" https://debt-simulator-login-backend.web.hsctf.com/yolo_0000000000001
{"response":"flag{y0u_f0uND_m3333333_123123123555554322221}"}
```
---

### Flag 
```
flag{y0u_f0uND_m3333333_123123123555554322221}
```
