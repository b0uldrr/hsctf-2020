## Ice Cream Bytes

* **CTF:** hsctf 2020 
* **Category:** binary
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 05/06/20

---

### Challenge
```
Introducing the new Ice Cream Bytes machine! Here’s a free trial: IceCreamBytes.java Oh, and make sure to read the user manual: IceCreamManual.txt

Author: wooshi
```

### Downloads
* [IceCreamBytes.java](IceCreamBytes.java)
* [IceCreamManual.txt](IceCreamManual.txt)

---

### Solution
I downloaded the challenge files. `IceCreamBytes.java` is a java class which prompts for user input, runs it through 4 different modification algorithms (`strawberryShuffle`, `vanillaShuffle`, `chocolateShuffle`, and `toppings`). It also reads the contents of the provided file `IceCreamManual.txt`, reads each character from the file, and then runs that through `fillMachine` to produce a 34 character array of bytes. The program then compares the output of our modified user input against the result of the `fillMachine` byte array and, if the two match, we know we gave the correct input. Our job is to reverse that process to find the correct input - the flag.

I modified `IceCreamBytes.java` to print out the output from `fillMachine`, as this would be the modified text that we needed to start from to retrieve the flag.

I then wrote a python script to reverse the process. Running it gave the flag.

```python
#! /usr/bin/python3

enc_flag = [108, 111, 108, 108, 111, 111, 107, 97, 116, 116, 104, 105, 115, 116, 101, 120, 116, 105, 103, 111, 116, 102, 114, 111, 109, 116, 104, 101, 109, 97, 110, 117, 97, 108]

toppings = [8, 61, -8, -7, 58, 55, -8, 49, 20, 65, -7, 54, -8, 66, -9, 69, 20, -9, -12, -4, 20, 5, 62, 3, -13, 66, 8, 3, 56, 47, -5, 13, 1, -7]

# Toppings
output1 = list(map(lambda x, y: x-y, enc_flag, toppings))

# Chocolate
output2 = [0] * len(output1)
for i in range(len(output1)-1, -1, -1):
    if(i % 2 ==0):
        if(i > 0):
            output2[i - 2] = output1[i]
        else:
            output2[len(output2) - 2] = output1[i]
    else:
        if(i < len(output1) - 2):
            output2[i + 2] = output1[i]
        else:
            output2[1] = output1[i]

# Vanilla
output3 = [0] * len(output2)
for i in range(0, len(output2)):
    if(i % 2 == 0):
        output3[i] = output2[i] - 1
    else:
        output3[i] = output2[i] + 1

# Strawberry
output3.reverse()

# Print the flag
print("flag{", end='')
for i in output3:
    print(chr(i), end='')
print("}")
```

---

### Flag 
```
flag{ic3_cr34m_byt3s_4r3_4m4z1n9_tr34ts}
```
