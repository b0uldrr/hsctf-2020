## Recursion Reverse

* **CTF:** hsctf 2020 
* **Category:** rev
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 05/06/20

---

### Challenge
```
Jimmy needs some help figuring out how computers process text, help him out!

Author: AD
```

### Downloads
[AscII.java](AscII.jvaa)

---

### Solution

The provided java source does the following:
1. Prompts for user input
1. Checks it is 12 chars in length
1. Steps through each character of the input string, transforming it with the `pickNum` function.
1. After all characters have been transformed, it reverses the string.
1. Checks whether the transformed + reversed string is equal to `I$N]=6YiVwC`. If so, then we know our original input was the flag.

Here is the source:

```java

import java.util.Scanner;
public class AscII {
	static int num = 0;
	
	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
		 System.out.print("Enter your  guess: ");
		 String guess = sc.nextLine();
		 
		 if (guess.length()!= 12) 
			 System.out.println("Sorry, thats wrong.");
		 else {
			 if(flagTransformed(guess).equals("I$N]=6YiVwC")) 
				 System.out.println("Yup the flag is flag{" + guess + "}");			 
			 else 
				 System.out.println("nope"); 
		 }
	}
	
	public static String flagTransformed(String guess) {
		char[] transformed = new char[12];
		
		for(int i = 0; i < 12; i++) {
			num = 1;
			transformed[i] = (char)(((int)guess.charAt(i) + pickNum(i + 1)) % 127);	
		}
		
		char[] temp = new char[12];		
		for(int i = 11; i >= 0; i--) 
			temp[11-i] = transformed[i];
			
		return new String(temp);	
	}
	
	private static int pickNum(int i) {
		
		for(int x = 0; x <= i; x++)
			num+=x;
		
		if(num % 2 == 0)
			return num;
		else 
			num = pickNum(num);
		
		return num;		
	}	 
}
```

I wrote a program to brute force the correct input by running every printable chracter (decimal 33 to 127) through the transform function and comparing the output to the transformed & reversed flag from the original source. Note that I re-reversed the flag to make it easier:

```java

import java.util.Scanner;
public class soln {
	static int num = 0;
	
	public static void main(String[] args) {
		 String encoded_flag = "CwViY6=]N$I";
		 for(int j=0; j<12; j++) {
		 	for(int i=32; i<127; i++) {
			 	if(flagTransformed((char)i, j) == encoded_flag.charAt(j)) {
				 	System.out.print((char)i);
			 	}
		 	}

		 }
	}
	
	public static char flagTransformed(char guess, int index) {
		char transformed;
	        num = 1;
		transformed = (char)(((int)guess + pickNum(index + 1)) % 127);	

		return transformed;
		
	}
	
	private static int pickNum(int i) {
		
		for(int x = 0; x <= i; x++)
			num+=x;
		
		if(num % 2 == 0)
			return num;
		else 
			num = pickNum(num);
		
		return num;		
	}	 
}
```

Compile with `javac soln.java` and run with `java soln`. It will print the flag.

---

### Flag 
```
flag{AscII is key}
```
