import java.util.Scanner;
public class soln {
	static int num = 0;
	
	public static void main(String[] args) {
		 String encoded_flag = "CwViY6=]N$I";
		 for(int j=0; j<12; j++) {
		 	for(int i=32; i<127; i++) {
			 	if(flagTransformed((char)i, j) == encoded_flag.charAt(j)) {
				 	System.out.print((char)i);
			 	}
		 	}

		 }
	}
	
	public static char flagTransformed(char guess, int index) {
		char transformed;
	        num = 1;
		transformed = (char)(((int)guess + pickNum(index + 1)) % 127);	

		return transformed;
		
	}
	
	private static int pickNum(int i) {
		
		for(int x = 0; x <= i; x++)
			num+=x;
		
		if(num % 2 == 0)
			return num;
		else 
			num = pickNum(num);
		
		return num;		
	}	 
}
