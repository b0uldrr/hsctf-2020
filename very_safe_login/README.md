## Traffic Lights

* **CTF:** hsctf 2020 
* **Category:** web
* **Points:** 100
* **Author(s):** b0uldrr
* **Date:** 05/06/20

---

### Challenge
```
Bet you can't log in.

https://very-safe-login.web.hsctf.com/very-safe-login

Author: Madeleine
```

---

### Solution

Challenge website presented a login form. The following javascript with the username and password was found in the source.

```javascript
<script>
    var login = document.login;
    
    function submit() {
        const username = login.username.value;
        const password = login.password.value;
            
        if(username === "jiminy_cricket" && password === "mushu500") {
            showFlag();
            return false;
        }
        return false;
    }
</script>
```

---

### Flag 
```
flag{cl13nt_51de_5uck5_135313531}
```
